from are_you_okay_with_this_combo.twitter_bot import TwitterBot
from are_you_okay_with_this_combo.combo_manager import ComboManager, FieldNames
from datetime import datetime, timedelta
import re
import argparse

POLL_LENGTH_IN_MINUTES = 1440


def new_poll(cards_in_combo):
    message = 'Are you okay with this combo in Commander?\n'
    for card in cards_in_combo:
        if card not in ['', None]:
            message += f'{card} + '
    message = re.sub(r' \+ $', '.\n', message)
    message += '#Magic #MTG #MTGCombo #Commander #EDH'

    return message


def arg_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', '--retweet', action='store_true')
    return parser.parse_args()


def main():
    bot = TwitterBot()
    card_manager = ComboManager()
    combo = card_manager.get_random_combo()
    args = arg_parser()

    if args.retweet:
        ongoing_polls = card_manager.ongoing_polls()
        for poll in ongoing_polls:
            bot.retweet(poll[FieldNames.POLL_ID])
    else:
        cards_in_combo = []
        for card in [
            combo[FieldNames.CARD_ONE],
            combo[FieldNames.CARD_TWO],
            combo[FieldNames.CARD_THREE],
            combo[FieldNames.CARD_FOUR]
        ]:
            if card not in ['', None]:
                cards_in_combo.append(card)

        response = bot.new_poll(message=new_poll(cards_in_combo), poll_duration_minutes=POLL_LENGTH_IN_MINUTES)
        combo[FieldNames.POLL_ID] = response.data['id']
        combo[card_manager.FIELD_NAMES[-1]] = (
                datetime.now() + timedelta(minutes=POLL_LENGTH_IN_MINUTES)
        ).strftime(card_manager.TIME_FORMAT)
        card_manager.update_line(combo)

        image_paths = []
        for card in cards_in_combo:
            image_paths.append(card_manager.download_card_image(card))

        bot.new_image_post(filenames=image_paths, reply_to=combo[FieldNames.POLL_ID])

        polls_ended = card_manager.polls_ended()
        for poll in polls_ended:
            poll_results = bot.get_poll_results(poll[FieldNames.POLL_ID])
            poll[FieldNames.ALWAYS] = poll_results['options'][0]['votes']
            poll[FieldNames.RULE_ZERO] = poll_results['options'][1]['votes']
            poll[FieldNames.NATURALLY] = poll_results['options'][2]['votes']
            poll[FieldNames.NEVER] = poll_results['options'][3]['votes']
            card_manager.update_line(poll)
        card_manager.delete_image_folder()


if __name__ == '__main__':
    main()

"https://twitter.com/nerdtothecore/status/1523669152183230465"

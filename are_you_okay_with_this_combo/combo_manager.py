import csv
import pathlib
import re
import requests
import scrython
import random
from datetime import datetime


class FieldNames:
    ID = 'id'
    CARD_ONE = 'card-one'
    CARD_TWO = 'card-two'
    CARD_THREE = 'card-three'
    CARD_FOUR = 'card-four'
    ALWAYS = 'yes-always'
    RULE_ZERO = 'if-discussed-in-rule-zero'
    NATURALLY = 'if-occurs-naturally'
    NEVER = 'never'
    POLL_ID = 'poll-id'
    POLL_END_TIME = 'poll-end-time'


class ComboManager:
    TIME_FORMAT = '%Y/%m/%d %H:%M:%S'
    TIME_FORMAT_MATCHER = r'^\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}$'
    CSV_PATH = f'{pathlib.Path(__file__).parent.parent}/data/combos.csv'
    IMAGES_PATH = f'{pathlib.Path(__file__).parent.parent}/data/images'
    FIELD_NAMES = [
        FieldNames.ID,
        FieldNames.CARD_ONE,
        FieldNames.CARD_TWO,
        FieldNames.CARD_THREE,
        FieldNames.CARD_FOUR,
        FieldNames.ALWAYS,
        FieldNames.RULE_ZERO,
        FieldNames.NATURALLY,
        FieldNames.NEVER,
        FieldNames.POLL_ID,
        FieldNames.POLL_END_TIME,
    ]

    def __init__(self):
        with open(self.CSV_PATH, 'r') as csv_file:
            combos = csv.DictReader(csv_file, delimiter=',')
            self.lines = []
            for line in combos:
                self.lines.append(line)

    @staticmethod
    def get_card_image(card_name):
        card = scrython.cards.Named(fuzzy=card_name)
        return card.image_uris()['normal']

    def download_card_image(self, card_name):
        pathlib.Path(self.IMAGES_PATH).mkdir(exist_ok=True)
        link = self.get_card_image(card_name)
        img_data = requests.get(link).content
        with open(f'{self.IMAGES_PATH}/{card_name}.jpg', 'wb') as handler:
            handler.write(img_data)
        return f'{self.IMAGES_PATH}/{card_name}.jpg'

    def get_random_combo(self):
        """
            Returns a line from the Combos csv file using the following rules:
                The combo does not have results from a previous poll.
                    Exception: There were 0 votes in the previous poll.
        :return: dict
        """
        choice = random.choice(self.lines)
        if choice[FieldNames.POLL_ID] not in [None, '']:
            choice = self.get_random_combo()
        else:
            if self.has_poll_results(choice):
                choice = self.get_random_combo()
        return choice

    def has_poll_results(self, line):
        return (self.is_acceptable_result(line[FieldNames.ALWAYS]) or
                self.is_acceptable_result(line[FieldNames.RULE_ZERO]) or
                self.is_acceptable_result(line[FieldNames.NATURALLY]) or
                self.is_acceptable_result(line[FieldNames.NEVER]))

    @staticmethod
    def is_acceptable_result(poll_option_result):
        return poll_option_result not in [None, 0, '']

    def update_line(self, new_line):
        for line in self.lines:
            if line[FieldNames.ID] == new_line[FieldNames.ID]:
                self.lines[self.lines.index(line)] = new_line
        self.save_lines()

    def save_lines(self):
        with open(self.CSV_PATH, 'w', newline='') as csv_file:
            combos = csv.DictWriter(csv_file, fieldnames=self.FIELD_NAMES, delimiter=',')
            combos.writeheader()
            combos.writerows(self.lines)
        with open(self.CSV_PATH, 'r', newline='') as csv_file:
            combos = csv.DictReader(csv_file, delimiter=',')
            self.lines = []
            for line in combos:
                self.lines.append(line)

    def get_polls(self, ongoing):
        polls = []
        now = datetime.now()
        for line in self.lines:
            if type(line[FieldNames.POLL_END_TIME]) == str:
                if re.search(self.TIME_FORMAT_MATCHER, line[FieldNames.POLL_END_TIME]):
                    poll_end_time = datetime.strptime(line[FieldNames.POLL_END_TIME], self.TIME_FORMAT)
                    if ongoing:
                        if now < poll_end_time and not self.has_poll_results(line):
                            polls.append(line)
                    else:
                        if now > poll_end_time and not self.has_poll_results(line):
                            polls.append(line)
        return polls

    def polls_ended(self):
        return self.get_polls(ongoing=False)

    def ongoing_polls(self):
        return self.get_polls(ongoing=True)

    def delete_image_folder(self):
        files = pathlib.Path(self.IMAGES_PATH).glob('*')
        for f in files:
            pathlib.Path(f).unlink()
        pathlib.Path(self.IMAGES_PATH).rmdir()


if __name__ == '__main__':
    c = ComboManager()
    combo = c.get_random_combo()
    c.download_card_image('Aetherwind Basker')
    c.delete_image_folder()

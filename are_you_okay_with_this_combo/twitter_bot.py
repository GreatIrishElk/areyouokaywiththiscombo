import time
import tweepy
import pathlib
import json


class TwitterBot:
    CONFIG_PATH = f"{pathlib.Path(__file__).parent.parent}/config.json"
    ACCESS_TOKEN_PATH = f"{pathlib.Path(__file__).parent.parent}/access_token.json"

    def __init__(self):
        with open(self.CONFIG_PATH, 'r') as config_file:
            self.config = json.load(config_file)
        self.access_tokens = None

        self.oauth2_handler = tweepy.OAuth2UserHandler(
            client_id=self.config['client_id'],
            redirect_uri=self.config['redirect_uri'],
            scope=['tweet.read', 'tweet.write', 'users.read', 'offline.access'],
            client_secret=self.config['client_secret']
        )
        self.load_access_tokens()
        self.client_auth2 = self.get_client()
        self.oauth1_handler = tweepy.OAuthHandler(
            consumer_key=self.config['consumer_key'],
            consumer_secret=self.config['consumer_secret']
        )
        self.oauth1_handler.set_access_token(
            key=self.config['access_token'],
            secret=self.config['access_token_secret']
        )
        self.client_auth1 = tweepy.API(self.oauth1_handler)

    def get_auth_url(self):
        return self.oauth2_handler.get_authorization_url()

    def enter_redirect_url(self):
        print(self.get_auth_url())

        return input('Paste redirect url here:  ')

    def load_access_tokens(self):
        if pathlib.Path(self.ACCESS_TOKEN_PATH).exists():
            with open(self.ACCESS_TOKEN_PATH, 'r') as access_token_file:
                self.access_tokens = json.load(access_token_file)
                if time.time() > float(self.access_tokens['expires_at']):
                    self.refresh_token()
        else:
            self.generate_access_tokens()
            self.load_access_tokens()

    def generate_access_tokens(self):
        response = self.oauth2_handler.fetch_token(self.enter_redirect_url())
        self.store_access_tokens(response)

    def store_access_tokens(self, response):
        with open(self.ACCESS_TOKEN_PATH, 'w') as access_token_file:
            json_object = json.dumps(
                {
                    'access_token': response['access_token'],
                    'refresh_token': response['refresh_token'],
                    'expires_at': response['expires_at']
                }
            )
            access_token_file.write(json_object)

    def refresh_token(self):
        if pathlib.Path(self.ACCESS_TOKEN_PATH).exists():
            response = self.oauth2_handler.refresh_token(
                token_url='https://api.twitter.com/2/oauth2/token',
                refresh_token=self.access_tokens['refresh_token']
            )
            self.store_access_tokens(response)
            self.load_access_tokens()
            self.client_auth2 = self.get_client()

    def get_client(self):
        return tweepy.Client(bearer_token=self.access_tokens['access_token'])

    def new_tweet(self, message, poll_duration_minutes=None, poll_options=None, user_auth=False):
        try:
            return self.client_auth2.create_tweet(
                text=message,
                poll_duration_minutes=poll_duration_minutes,
                poll_options=poll_options,
                user_auth=user_auth
            )
        except tweepy.errors.Unauthorized:
            self.refresh_token()

    def retweet(self, tweet_id):
        self.client_auth2.retweet(tweet_id, user_auth=False)

    def new_poll(self, message, poll_duration_minutes=1440, poll_options=None, user_auth=False):
        if not poll_options:
            poll_options = ['Yes, Always', 'If Discussed In Rule Zero', 'If Occurs Naturally', 'Never']
        return self.new_tweet(
            message=message,
            poll_duration_minutes=poll_duration_minutes,
            poll_options=poll_options,
            user_auth=user_auth
        )

    def new_image_post(self, filenames, message=None, reply_to=None):
        if reply_to:
            if not message:
                message = '@OkayCombo'
        uploaded_media = []
        if type(filenames) is list:
            for filename in filenames:
                uploaded_media.append(self.client_auth1.media_upload(filename).media_id)
        self.client_auth1.update_status(status=message, in_reply_to_status_id=reply_to, media_ids=uploaded_media)

    def get_tweet(self, tweet_id):
        self.client_auth2.get_tweet(id=tweet_id)

    def get_poll_results(self, tweet_id):
        response = self.client_auth2.get_tweet(
            id=tweet_id,
            expansions='attachments.poll_ids',
            poll_fields='duration_minutes,end_datetime,options,voting_status'
        )
        return response.includes['polls'][0]

    @staticmethod
    def acceptable_tweet_length(message):
        return len(message) <= 240
